package quadtree;

import quadtree.gjk.Vector.AXIS_X;
import quadtree.gjk.Vector.AXIS_Y;
import quadtree.gjk.Vector;
import quadtree.helpers.CollisionResult.ObjectId;
import quadtree.helpers.CollisionResult;
import quadtree.helpers.Overlap;
import quadtree.types.Collider;
import quadtree.types.MovingPoint;
import quadtree.types.MovingRectangle;
import quadtree.types.Rectangle;

using quadtree.extensions.ColliderEx;
using quadtree.extensions.RectangleEx;
using quadtree.helpers.MathUtils;


@:access(quadtree.helpers.CollisionResult)
class Physics
{
    /**
        Separates the two given overlapping objects, based on their movement.

        @param collisionResult The collision result. This object will be modified in-place to contain information about the separation.
    **/
    public static function separate(collisionResult: CollisionResult)
    {
        computeOverlap(collisionResult);
        performSeparation(collisionResult);
    }


    /**
        Updates the given CollisionResult with the overlap of the two objects on each axis.
    **/
    public static function computeOverlap(collisionResult: CollisionResult)
    {
        // Default case, find overlap from movement.
        final obj1: Collider = collisionResult.object1;
        final obj2: Collider = collisionResult.object2;

        var overlapX: Int = computeOverlapOnAxis(obj1, obj2, AXIS_X);
        var overlapY: Int = computeOverlapOnAxis(obj1, obj2, AXIS_Y);

        collisionResult.addOverlap(overlapX, overlapY);
    }


    /**
        Similar to `Physics.separate()`, but used to separate one object from multiple others.
        An example where this might be useful is tilemaps, where a hitbox may overlap with multiple
        tiles at the same time. In such cases, this function will attempt to find the way to separate
        it from all the tiles at once with the minimum correction in position.

        @param collisionResult The collision result.
        @param objectId Whether the single object being separated is object0 or object1 of the collision result.
        @param otherObjects The array of objects, from which to separate the object specified by `objectId`.
    **/
    public static function separateFromMultiple(collisionResult: CollisionResult, objectId: ObjectId, otherObjects: Array<Collider>)
    {
        final originalObj1: Collider = collisionResult.object1;
        final originalObj2: Collider = collisionResult.object2;

        var obj1: Collider = originalObj1;
        var obj2: Collider = originalObj2;

        for (otherObj in otherObjects)
        {
            switch objectId
            {
                case Object1: obj2 = otherObj;
                case Object2: obj1 = otherObj;
            }

            collisionResult.setObjects(obj1, obj2);
            computeOverlap(collisionResult);
        }

        collisionResult.setObjects(originalObj1, originalObj2);

        performSeparation(collisionResult);
    }


    static function performSeparation(collisionResult: CollisionResult)
    {
        final obj1: Collider = collisionResult.object1;
        final obj2: Collider = collisionResult.object2;

        final obj1canMove: Bool = collisionResult.canObject1Move();
        final obj2canMove: Bool = collisionResult.canObject2Move();

        var overlapX: Int = collisionResult.overlapX;
        var overlapY: Int = collisionResult.overlapY;

        if (overlapX.isNonZero() && overlapY.isNonZero())
        {
            // We have overlap on both axes.
            // Check if we can ignore one of them and settle for a small
            // correction on the other.
            // (for example when touching the ground, you only want to correct upwards)
            if (overlapX.isNonZero() && Math.abs(overlapX / overlapY) < 0.5)
            {
                // Significantly smaller margin on the x-axis, use only that for separation.
                collisionResult.overlapY = overlapY = 0;
            }
            else if (overlapY.isNonZero() && Math.abs(overlapY / overlapX) < 0.5)
            {
                // Significantly smaller margin on the y-axis, use only that for separation.
                collisionResult.overlapX = overlapX = 0;
			}
        }

        var separationHappened: Bool = false;
        if (overlapX.isZero() && overlapY.isZero())
        {
            // No overlap, do nothing.
        }
        else if (obj1canMove && obj2canMove)
        {
            obj1.moveToSeparate(-Std.int(overlapX / 2), -Std.int(overlapY / 2));
            obj2.moveToSeparate( Std.int(overlapX / 2),  Std.int(overlapY / 2));
            separationHappened = true;
        }
        else if (!obj2canMove)
        {
            obj1.moveToSeparate(-overlapX, -overlapY);
            separationHappened = true;
        }
        else if (!obj1canMove)
        {
            obj2.moveToSeparate(overlapX, overlapY);
            separationHappened = true;
        }

        collisionResult.addSeparation(overlapX, overlapY, separationHappened);
    }

    /**
        Computes the overlap of two given objects on the given axis.
        This function assumes that the objects are moveable and bases the overlap on
        their last movement.

        This overlap should be subtracted from `obj1` and added to `obj2`.
    **/
    static function computeOverlapOnAxis(obj1: Collider, obj2: Collider, axis: Int): Int
    {
        // Calculate overlap based on movement.
        var delta1: Int = obj1.getObjectDelta(axis);
        var delta2: Int = obj2.getObjectDelta(axis);
        var delta: Int = delta1 - delta2;
        var overlap: Int = delta;

		return Overlap.alignedRectangleInAlignedRectangle(cast obj1, cast obj2, axis, delta);
    }
}
