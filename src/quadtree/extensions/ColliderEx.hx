package quadtree.extensions;

import quadtree.gjk.Vector.AXIS_X;
import quadtree.gjk.Vector.AXIS_Y;
import quadtree.helpers.BoundingBox;
import quadtree.types.Collider;
import quadtree.types.MovingPoint;
import quadtree.types.MovingRectangle;
import quadtree.types.Rectangle;

using quadtree.extensions.MovingRectangleEx;
using quadtree.extensions.RectangleEx;
using quadtree.helpers.MathUtils;


class ColliderEx
{
    public static inline function getObjectDelta(obj: Collider, axis: Int): Int
    {
        return switch [obj.areaType, axis]
        {
            case [MovingRectangle, AXIS_X]: cast(obj, MovingRectangle).x - cast(obj, MovingRectangle).lastX;
            case [MovingRectangle, AXIS_Y]: cast(obj, MovingRectangle).y - cast(obj, MovingRectangle).lastY;
            
            case [MovingPoint, AXIS_X]: cast(obj, MovingPoint).x - cast(obj, MovingPoint).lastX;
            case [MovingPoint, AXIS_Y]: cast(obj, MovingPoint).y - cast(obj, MovingPoint).lastY;
            case _: 0;
        }
    }


    public static inline function isAlignedRectangle(obj: Collider): Bool
    {
        return obj.areaType & (CollisionAreaType.Rectangle | CollisionAreaType.MovingRectangle) > 0;
    }


    public static inline function isMovableType(obj: Collider): Bool
    {
        return obj.areaType & (CollisionAreaType.MovingPoint | CollisionAreaType.MovingRectangle) > 0;
    }


    /**
        Calculates a rectangular bounding box around the collider, and stores it in the given `result` instance.

        @return Returns `true` if a bounding box exists for the collider.
    **/
    public static inline function getBoundingBox(obj: Collider, result: BoundingBox): Bool
    {
        var exists: Bool = true;
        switch obj.areaType
        {
            case Rectangle: cast(obj, Rectangle).getBoundingBox(result);

            case MovingRectangle: cast(obj, MovingRectangle).getBoundingBox(result);

            case _: exists = false;
        }
        return exists;
    }
}
