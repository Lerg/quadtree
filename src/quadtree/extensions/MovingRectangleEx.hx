package quadtree.extensions;

import quadtree.gjk.Gjk;
import quadtree.gjk.Vector;
import quadtree.helpers.BoundingBox;
import quadtree.types.Collider;
import quadtree.types.MovingPoint;
import quadtree.types.MovingRectangle;
import quadtree.types.Point;
import quadtree.types.Rectangle;

using quadtree.extensions.MovingRectangleEx;
using quadtree.helpers.MathUtils;


class MovingRectangleEx
{
    public static inline function intersectsWith(rect: MovingRectangle, hullX: Int, hullY: Int, hullWidth: Int, hullHeight: Int, other: Collider, gjk: Gjk): Bool
    {
        return switch other.areaType
        {
            case CollisionAreaType.Point: 
                intersectsWithPoint(hullX, hullY, hullWidth, hullHeight, cast(other, Point).x, cast(other, Point).y);

            case CollisionAreaType.Rectangle: intersectsWithRectangle(hullX, hullY, hullWidth, hullHeight, cast(other, Rectangle));

            case CollisionAreaType.MovingRectangle: intersectsWithMovingRectangle(hullX, hullY, hullWidth, hullHeight, cast(other, MovingRectangle));

            case _: gjk.checkOverlap(rect, other);
        }
    }


    public static inline function intersectsWithPoint(hullX: Int, hullY: Int, hullWidth: Int, hullHeight: Int, pointX: Int, pointY: Int): Bool
    {
        return PointEx.intersectsWithRectangle(pointX, pointY, hullX, hullY, hullWidth, hullHeight);
    }


    public static inline function intersectsWithRectangle(hullX: Int, hullY: Int, hullWidth: Int, hullHeight: Int, other: Rectangle): Bool
    {
        return hullX + hullWidth  > other.x
            && hullY + hullHeight > other.y
            && hullX              < other.x + other.width
            && hullY              < other.y + other.height;
    }


    public static inline function intersectsWithMovingRectangle(hullX: Int, hullY: Int, hullWidth: Int, hullHeight: Int, other: MovingRectangle): Bool
    {
        return hullX + hullWidth  > other.hullX()
            && hullY + hullHeight > other.hullY()
            && hullX              < other.hullX() + other.hullWidth()
            && hullY              < other.hullY() + other.hullHeight();
    }

    
    public static inline function getFarthestPointInDirection(rect: MovingRectangle, direction: Vector, result: Vector): Vector
    {
        return RectangleEx.getFarthestPointInDirectionRect(rect.hullX(), rect.hullY(), rect.hullWidth(), rect.hullHeight(), direction, result);
    }


    public static inline function hullX(rect: MovingRectangle): Int
    {
        return MathUtils.imin(rect.x, rect.lastX);
    }
    

    public static inline function hullY(rect: MovingRectangle): Int
    {
        return MathUtils.imin(rect.y, rect.lastY);
    }
    

    public static inline function hullWidth(rect: MovingRectangle): Int
    {
        return rect.width + MathUtils.iabs(rect.x - rect.lastX);
    }
    

    public static inline function hullHeight(rect: MovingRectangle): Int
    {
        return rect.height + MathUtils.iabs(rect.y - rect.lastY);
    }


    public static inline function getBoundingBox(r: MovingRectangle, result: BoundingBox)
    {
        result.x = r.hullX();
        result.y = r.hullY();
        result.width = r.hullWidth();
        result.height = r.hullHeight();
    }
}
