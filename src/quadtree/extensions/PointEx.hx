package quadtree.extensions;

import quadtree.gjk.Gjk;
import quadtree.gjk.Vector;
import quadtree.types.Collider;
import quadtree.types.MovingPoint;
import quadtree.types.MovingRectangle;
import quadtree.types.Point;
import quadtree.types.Rectangle;

using quadtree.extensions.MovingRectangleEx;
using quadtree.helpers.MathUtils;


class PointEx
{
    public static inline function intersectsWith(point: Point, other: Collider, gjk: Gjk): Bool
    {
        return switch other.areaType
        {
            case CollisionAreaType.Point: intersectsWithPoint(point, cast(other, Point));

            case CollisionAreaType.MovingPoint: intersectsWithMovingPoint(point, cast(other, MovingPoint));

            case CollisionAreaType.Rectangle: intersectsWithRectangle(point.x, point.y, cast(other, Rectangle).x, cast(other, Rectangle).y, cast(other, Rectangle).width, cast(other, Rectangle).height);

            case CollisionAreaType.MovingRectangle: intersectsWithMovingRectangle(point, cast(other, MovingRectangle));

            case _: gjk.checkOverlap(point, other);
        }
    }


    public static inline function intersectsWithPoint(point: Point, other: Point): Bool
    {
        return false;
    }


    public static inline function intersectsWithMovingPoint(point: Point, other: MovingPoint): Bool
    {
        return false;
    }


    public static function intersectsWithRectangle(pointX: Int, pointY: Int, x: Int, y: Int, width: Int, height: Int): Bool
    {
        return pointX >= x
            && pointY >= y
            && pointX < x + width
            && pointY < y + height;
    }


    public static inline function intersectsWithMovingRectangle(point: Point, other: MovingRectangle): Bool
    {
        return intersectsWithRectangle(point.x, point.y, other.hullX(), other.hullY(), other.hullWidth(), other.hullHeight());
    }


    public static inline function getFarthestPointInDirection(point: Point, direction: Vector, result: Vector): Vector
    {
        return result.set(point.x, point.y);
    }
}
