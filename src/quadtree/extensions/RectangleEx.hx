package quadtree.extensions;

import quadtree.gjk.Gjk;
import quadtree.gjk.Vector;
import quadtree.helpers.BoundingBox;
import quadtree.helpers.CollisionResult;
import quadtree.types.Collider;
import quadtree.types.MovingPoint;
import quadtree.types.MovingRectangle;
import quadtree.types.Point;
import quadtree.types.Rectangle;

using quadtree.extensions.MovingPointEx;
using quadtree.extensions.MovingRectangleEx;
using quadtree.extensions.PointEx;
using quadtree.helpers.MathUtils;


class RectangleEx
{
    public static inline function intersectsWith(rect: Rectangle, other: Collider, gjk: Gjk): Bool
    {
        return switch other.areaType
        {
            case CollisionAreaType.Point:
                intersectsWithPoint(rect, cast(other, Point));

            case CollisionAreaType.MovingPoint:
                intersectsWithMovingPoint(rect, cast(other, MovingPoint));

            case CollisionAreaType.Rectangle: intersectsWithRectangle(rect, cast(other, Rectangle));

            case CollisionAreaType.MovingRectangle: intersectsWithMovingRectangle(rect, cast(other, MovingRectangle));

            case _: gjk.checkOverlap(rect, other);
        }
    }


    public static inline function intersectsWithPoint(rect: Rectangle, other: Point): Bool
    {
        return PointEx.intersectsWithRectangle(other.x, other.y, rect.x, rect.y, rect.width, rect.height);
    }


    public static inline function intersectsWithMovingPoint(rect: Rectangle, other: MovingPoint): Bool
    {
        return cast(other, MovingPoint).intersectsWithRectangle(rect);
    }


    public static inline function intersectsWithRectangle(rect: Rectangle, other: Rectangle): Bool
    {
        return rect.x + rect.width  > other.x
            && rect.y + rect.height > other.y
            && rect.x               < other.x + other.width
            && rect.y               < other.y + other.height;
    }


    public static inline function intersectsWithMovingRectangle(rect: Rectangle, other: MovingRectangle): Bool
    {
        return MovingRectangleEx.intersectsWithRectangle(other.hullX(), other.hullY(), other.hullWidth(), other.hullHeight(), rect);
    }


    public static inline function getFarthestPointInDirection(rect: Rectangle, direction: Vector, result: Vector): Vector
    {
        return getFarthestPointInDirectionRect(rect.x, rect.y, rect.width, rect.height, direction, result);
    }


    public static function getFarthestPointInDirectionRect(x: Int, y: Int, width: Int, height: Int, direction: Vector, result: Vector): Vector
    {
		var distanceToTopLeft: Int = direction.dot(x, y);
		var distanceToTopRight: Int  = direction.dot(x + width, y);
		var distanceToBotLeft: Int = direction.dot(x, y + height);
		var distanceToBotRight: Int  = direction.dot(x + width, y + height);

		if (distanceToTopLeft > distanceToTopRight && distanceToTopLeft > distanceToBotLeft && distanceToTopLeft > distanceToBotRight)
		{
			// top left
			result.set(x, y);
		}
		else if (distanceToTopRight > distanceToBotLeft && distanceToTopRight > distanceToBotRight)
		{
			// top right
			result.set(x + width, y);
		}
		else if (distanceToBotLeft > distanceToBotRight)
		{
			// bot left
			result.set(x, y + height);
		}
		else
		{
			// bot right
			result.set(x + width, y + height);
		}

        return result;
    }


    public static inline function getBoundingBox(r: Rectangle, result: BoundingBox)
    {
        result.x = r.x;
        result.y = r.y;
        result.width = r.width;
        result.height = r.height;
    }


    /**
        Returns `true` if the rectanlge contains the point `(0, 0)`.
    **/
    public static inline function containsOrigin(r: Rectangle): Bool
    {
        return PointEx.intersectsWithRectangle(0, 0, r.x, r.y, r.width, r.height);
    }
}
