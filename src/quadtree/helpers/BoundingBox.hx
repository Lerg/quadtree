package quadtree.helpers;

import quadtree.types.Collider;
import quadtree.types.Rectangle;


class BoundingBox implements Rectangle
{
    public final areaType: CollisionAreaType = CollisionAreaType.Rectangle;
    public final collisionsEnabled: Bool = false;
    public final angle: Float = 0;
    
    public var x: Int;
    public var y: Int;
    public var width: Int;
    public var height: Int;
	public var categoryBits = 0;
	public var maskBits = 0;

    // For caching them.
    @:noCompletion
    public var next: BoundingBox;


    public function new(x: Int, y: Int, width: Int = 0, height: Int = 0)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }


    @IgnoreCover
    public function onOverlap(other: Collider): Void { }
    

    @IgnoreCover
    public function moveToSeparate(deltaX: Int, deltaY: Int) { }
}
