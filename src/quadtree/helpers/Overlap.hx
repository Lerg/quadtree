package quadtree.helpers;

import quadtree.gjk.Vector.AXIS_X;
import quadtree.gjk.Vector.AXIS_Y;
import quadtree.types.Rectangle;

using quadtree.helpers.MathUtils;


class Overlap
{

    public static function alignedRectangleInAlignedRectangle(r1: Rectangle, r2: Rectangle, axis: Int, delta: Int): Int
    {
        return switch axis
        {
            // |-x----- r1 -> r2 ---------
            case AXIS_X if (delta > 0): r1.x + r1.width - r2.x;

            // |-x----- r2 <- r1 ---------
            case AXIS_X if (delta < 0): r1.x - r2.x - r2.width;

            // No delta info, get the smallest side.
            case AXIS_X: MathUtils.iminAbs(r1.x + r1.width - r2.x, r1.x - r2.x - r2.width);

            // |-y----- r1 -> r2 ---------
            case AXIS_Y if (delta > 0): r1.y + r1.height - r2.y;

            // |-y----- r2 <- r1 ---------
            case AXIS_Y if (delta < 0): r1.y - r2.y - r2.height;

            // No delta info, get the smallest side.
            case AXIS_Y: MathUtils.iminAbs(r1.y + r1.height - r2.y, r1.y - r2.y - r2.height);

            case _: 0;
        }
    }
}
