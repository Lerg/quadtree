package quadtree.types;


interface MovingRectangle extends Rectangle
{
    /** The x-coordinate of the rectangle on the previous frame. **/
    public var lastX(default, never): Int;
    
    
    /** The y-coordinate of the rectangle on the previous frame. **/
    public var lastY(default, never): Int;
}
