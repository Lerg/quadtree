package quadtree.types;


interface Point extends Collider
{
    /** The x-coordinate of the point. **/
    public var x(default, never): Int;
    

    /** The y-coordinate of the point. **/
    public var y(default, never): Int;
}
