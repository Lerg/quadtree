package quadtree.types;


interface Rectangle extends Point
{
    /** The width of the rectangle. **/
    public var width(default, never): Int;
    
    
    /** The height of the rectangle. **/
    public var height(default, never): Int;
}
